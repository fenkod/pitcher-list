#include <Rcpp.h>
using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//

// [[Rcpp::export]]
public static VPRType CalculateGameVPRValue(decimal? inningsPitched, decimal? era)
{
  if (inningsPitched >= 6 && era <= 3.00M)
  {
    return VPRType.Excellent;
  }
  else if (era > 4.5M)
  {
    return VPRType.Poor;
  }
  else
  {
    return VPRType.Neutral;
  }
}

public static kVPRType CalculateGamekVPRValue(decimal? whip, decimal? strikeouts)
{
  if (whip <= .75M)
  {
    if (strikeouts >= 5)
    {
      return kVPRType.Excellent;
    }
    else
    {
      return kVPRType.Neutral;
    }
  }
  else if (strikeouts >= 9)
  {
    if (whip <= 1.2M)
    {
      return kVPRType.Excellent;
    }
    else if (whip <= 1.75M)
    {
      return kVPRType.Neutral;
    }
    else
    {
      return kVPRType.Poor;
    }
  }
  else if (whip <= 1.15M)
  {
    if (strikeouts >= 7)
    {
      return kVPRType.Excellent;
    }
    else
    {
      return kVPRType.Neutral;
    }
  }
  else if (whip <= 1.2M && strikeouts >= 9)
  {
    return kVPRType.Excellent;
  }
  else if ((strikeouts > 4 && strikeouts < 7) || (whip > 1.15M && whip <= 1.4M))
  {
    return kVPRType.Neutral;
  }
  else
  {
    return kVPRType.Poor;
  }
}


// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically 
// run after the compilation.
//

/*** R
timesTwo(42)
*/
