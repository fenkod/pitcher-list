# Setup the environment

source("scripts/ensureDependencies.R")
source("credentials/yahoo_api.R")
check.packages("pacman")
p_load(httr)

myapp <- oauth_app("yahoo", 
                   key = Sys.getenv("YAHOO_KEY"), 
                   secret = Sys.getenv("YAHOO_SECRET")
                   )

yahoo_token <- oauth2.0_token(oauth_endpoints("yahoo"),
                              myapp,
                              use_oob = TRUE,
                              oob_value = "oob"
                              )


yahoo_config <- config(token = yahoo_token)

league_ids <- c(133706, 139142, 159888, 148706, 157728, 136490)
names(league_ids) <- c("Legacy", "Prodigy", "Futures", 
                       "PL 1", "PL 2", "Bottom of the Barrel")

yahoo_base_url <- "https://fantasysports.yahooapis.com/fantasy/v2/leagues"
mlb_game_id <- "mlb.l"
sub_resource <- "/scoreboard"
response_type <- "?response=json"

leagues_resource <- ""

for (league in league_ids) {
  leagues_resource <- paste(
    paste(
      mlb_game_id,
      league,
      sep = "."
      ),
    leagues_resource,
    sep = ",")
}

leagues_resource <- substr(leagues_resource, 1, nchar(leagues_resource)-1)

yahoo_url = paste(
  yahoo_base_url,
  ";league_keys=",
  leagues_resource,
  sub_resource,
  response_type,
  sep = ""
)

res <- GET(yahoo_url, yahoo_config)