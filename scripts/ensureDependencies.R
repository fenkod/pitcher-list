# check.packages function: install and load multiple R packages.
# check.packages.github function: install and load multiple R packages only on GitHub
# Check to see if packages are installed. Install them if they are not, then load them into the R session.

check.packages <- function(pkg){
  new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
  if (length(new.pkg)) 
    install.packages(new.pkg, dependencies = TRUE)
  sapply(pkg, require, character.only = TRUE)
}


check.packages.github <- function(pkg, src){
  
  # Need to determine if an element is not in the installed.packages in an if block.
  # Reverses the %in% function.
  '%ni%' <- Negate('%in%')
  
  for (i in 1:length(pkg)){
    if (pkg[i] %ni% installed.packages()[, "Package"]) {
      devtools::install_github(src[i])
    }
  }
  
  sapply(pkg, require, character.only = TRUE)
}