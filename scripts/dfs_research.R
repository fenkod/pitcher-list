source("credentials/msf_api.R")
# source("scripts/fantasyBenchmarks.R")
library(mysportsfeedsR)

authenticate_v2_x(msf.api.key)

# Daily Pull -- Fanduel
# dfs.june.22.2018 <- dfs.2018 <- msf_get_results(version = '2.0', 
#                                                 league = 'mlb', 
#                                                 season = '2018-regular', 
#                                                 feed = 'daily_dfs', 
#                                                 params = list(
#                                                   dfstype = 'draftkings', 
#                                                   date = '20180622'
#                                                   )
#                                                 )
# dfs.june.22.2018 <- dfs.june.22.2018$api_json$dfsEntries$dfsRows[[1]]

team_list <- c("tb", "nyy", "tor", "bos", "bal",
               "min", "cle", "det", "cws", "kc",
               "hou", "sea", "tex", "oak", "laa",
               "phi", "nym", "atl", "was", "mia",
               "stl", "pit", "chc", "mil", "cin",
               "lad", "sd", "ari", "sf", "col")

current_season <- 2019

dfs.season <- data.frame()

for (team in team_list) {
  dfs.season.raw <- msf_get_results(version = '2.0', 
                                    league = 'mlb', 
                                    season = paste(current_season, "-regular", sep = ""), 
                                    feed = 'seasonal_dfs', 
                                    params = list(
                                      dfstype = 'draftkings', 
                                      team = team
                                    )
  )
  dfs.season.raw <- as.data.frame(dfs.season.raw$api_json$dfsEntries$dfsRows[[1]])
  
  dfs.season <- rbind(dfs.season, dfs.season.raw)
  
  Sys.sleep(60)
}

write.csv(dfs.season, 
          paste(
            "output/",
            current_season,
            "_draftkings.csv",
            sep = ""
          )
          )
